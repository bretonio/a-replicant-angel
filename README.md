# Blade Runner-Themed

> An Angular brinquedo to deploy. Deploy to Gitlab Pages properly. No blank page in root.  With Material Design Components. More later soon.



## TestUi

[This project](https://gitlab.com/rtsdg/angular-with-material-ui) was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.1 and made avaiable by [this generous developer](https://gitlab.com/rtsdg). Cloned in Sambodia and themed and elaborated by @bretonio in San Pavlov City.


## Development Server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code Scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running Unit Tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running End-to-End Tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further Help

For theming Angular Material projects see [official docs](https://material.angular.io/guide/theming) and [Tomas Trajan](https://medium.com/@tomastrajan/the-complete-guide-to-angular-material-themes-4d165a9d24d1).

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
